package me.kazuho;

import com.google.common.base.Optional;

public class TestGuavaOptional {
  public static void main(String[] args) {
    Integer result;

    Integer nullVal = null;
    Integer ten = 10;
    result = normalSum(nullVal, ten);
    System.out.println("normalSum - result: " + result);

    Optional<Integer> a = Optional.fromNullable(nullVal);
    Optional<Integer> b = Optional.fromNullable(ten);
    result = guavaSum(a, b);
    System.out.println("guavaSUm - result: " + result);
  }


  private static Integer normalSum(Integer a, Integer b) {
    //  using guava-optional before
    if (a == null) {
      System.out.println("a is null.");
      a = 0;
    }

    if (b == null) {
      System.out.println("b is null.");
      b = 0;
    }

    return a + b;
  }

  private static Integer guavaSum(Optional<Integer> a, Optional<Integer> b){
    //  detect if parameter is null
    System.out.println("a.isPresent(): " + a.isPresent());
    System.out.println("b.isPresent(): " + b.isPresent());
    //  using guava-optional after
    return a.or(0) + b.or(0);
  }
}
