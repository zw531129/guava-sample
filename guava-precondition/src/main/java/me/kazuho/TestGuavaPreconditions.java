package me.kazuho;

import com.google.common.base.Preconditions;

public class TestGuavaPreconditions {
  public static void main(String[] args) {
    TestGuavaPreconditions guava = new TestGuavaPreconditions();

    try {
      System.out.println(guava.sqrt(-3.0));
    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
    try {
      System.out.println(guava.sum(null, 3));
    } catch (NullPointerException e) {
      System.out.println(e.getMessage());
    }
    try {
      Integer[] intArr = new Integer[]{1, 2, 3, 4, 5};
      System.out.println(guava.getValue(intArr, 7));
    } catch (IndexOutOfBoundsException e) {
      System.out.println(e.getMessage());
    }
  }

  public Double sqrt(double input) {
    /**
     if (input <= 0){
     throw new IllegalArgumentException("Negative value " + input + " passed");
     }
     **/
    //  check parameter by statement
    Preconditions.checkArgument(input > 0,
            "Negative value %s passed",
            input);

    return Math.sqrt(input);
  }

  public Integer sum(Integer a, Integer b) {
    /**
     if (null == a){
     throw new NullPointerException("Nullable value " + a + " passed");
     }

     if (null == b){
     throw new NullPointerException("Nullable value " + b + " passed");
     }
     **/
    //  check null parameter
    a = Preconditions.checkNotNull(a, "Nullable value %s passed", a);
    b = Preconditions.checkNotNull(b, "Nullable value %s passed", b);

    return a + b;
  }

  public <T> T getValue(T[] arr, int index) {
    if (index >= arr.length) {
      throw new IndexOutOfBoundsException("index " + index + " must be less than size " + arr.length);

    }
    //  check out of bound
    Preconditions.checkElementIndex(index, arr.length);
    return arr[index];
  }
}
