package me.kazuho;

import com.google.common.collect.Range;

public class TestGuavaRange {
  public static void main(String[] args) {
    TestGuavaRange guava = new TestGuavaRange();
    guava.showRange();
  }

  public void showRange(){
    //  {x | a <= x <= b}
    Range<Integer> intRange = Range.closed(0, 8);
    //  {x | a <= x < b}
    Range<Long> longRange = Range.closedOpen(16L, 32L);
    //  {x | a < x <= b}
    Range<Double> doubleRange = Range.openClosed(0.0, 8.0);
    //  {x | a < x < b}
    Range<Float> floatRange = Range.open(0.0F, 16.0F);
    //  create an open ended range (9, infinity)
    Range<Integer> greaterThan = Range.greaterThan(8);
    //  {x | x >= a}
    Range<Integer> atLeast = Range.atLeast(0);
    //  {x | x < b}
    Range<Long> lessThan = Range.lessThan(16L);
    //  {x | x <= b}
    Range<Long> atMost = Range.atMost(32L);
    //  (-∞..+∞)
    Range<Integer> all = Range.all();
    //
    System.out.println("Has lower bound? " + greaterThan.hasLowerBound());
    System.out.println("Lower Bound: " + greaterThan.lowerEndpoint());
    System.out.println("Has upper bound? " + greaterThan.hasUpperBound());
    System.out.println("Upper Bound: " + (greaterThan.hasUpperBound() ? greaterThan.upperEndpoint() : "false"));
    //
    System.out.println("atLeast enclose intRange? " + atLeast.encloses(intRange));
    //
    System.out.println("lessThan connect longRange? " + lessThan.encloses(longRange));
    //
    System.out.println("atMost intersect longRange? " + atMost.intersection(longRange));
    //
    System.out.println("atLeast span greaterThan: " + atLeast.span(greaterThan));
  }
}
