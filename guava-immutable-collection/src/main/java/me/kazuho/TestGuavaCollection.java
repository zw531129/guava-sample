package me.kazuho;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.UnmodifiableIterator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestGuavaCollection {
  //  init immutable set by ImmutableSet.of() function
  static final ImmutableSet<String> COLORS = ImmutableSet.of(
          "red",
          "orange",
          "yellow",
          "green",
          "blue",
          "purple");

  public static void main(String[] args) {
    //  list view
    List<String> colorList = COLORS.asList();
    System.out.println(colorList.toString());

    //  copy of JDK Set
    Set<String> carSet = new HashSet<>();
    carSet.add("FORD");
    carSet.add("BUICK");
    ImmutableSet<String> copyOfCarSet = ImmutableSet.copyOf(carSet);
    System.out.println(copyOfCarSet);

    //  hashcode
    System.out.println(copyOfCarSet.hashCode());

    //  iterate
    UnmodifiableIterator<String> iterator = copyOfCarSet.iterator();
    while (iterator.hasNext()) {
      System.out.println(iterator.next());
    }

  }
}
