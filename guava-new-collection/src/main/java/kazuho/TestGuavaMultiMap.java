package kazuho;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestGuavaMultiMap {
  public static final String GOOGLE = "www.google.com";

  public static void main(String[] args) {
    //  init data
    Map<String, List<User>> pvMap = new HashMap<>();
    List<User> users = new ArrayList<>();
    users.add(new User("Joe", 7));
    users.add(new User("Yuri", 56));
    pvMap.put(GOOGLE, users);
    //  traditional way
    countPvByHashMap(pvMap, GOOGLE);

    System.out.println("----------------------------------------");

    //  init data
    Multimap<String, User> pvMultiMap = ArrayListMultimap.create();
    pvMultiMap.put(GOOGLE, new User("Joe", 7));
    pvMultiMap.put(GOOGLE, new User("Yuri", 56));
    //  guava way
    countPvByMultiMap(pvMultiMap, GOOGLE);

    //  log complex data
    fruits();
  }

  public static void fruits() {
    Multimap<String, String> fruits = ArrayListMultimap.create();
    fruits.put("Fruits", "Banana");
    fruits.put("Fruits", "Apple");
    fruits.put("Fruits", "Pear");
    fruits.put("Fruits", "Pear");
    fruits.put("Vegetables", "Carrot");

    System.out.println("Fruits contains: " + fruits.size() + " fruit.");
    System.out.println("Fruits: " + fruits.get("Fruits"));
    System.out.println("Vegetables: " + fruits.get("Vegetables"));

  }

  public static void countPvByHashMap(Map<String, List<User>> pvMap, String url) {
    List<User> users = pvMap.get(url);
    if (users == null) {
      users = new ArrayList<>();
      pvMap.put(url, users);
    }

    for (User u : users) {
      System.out.println("User: " + u.name + " visits " + url + " " + u.pv + " times.");
    }
  }

  public static void countPvByMultiMap(Multimap<String, User> pvMultiMap, String url) {
    Collection<User> users = pvMultiMap.get(GOOGLE);
    for (User u : users) {
      System.out.println("User: " + u.name + " visits " + url + " " + u.pv + " times.");
    }
  }
}

class User {
  String name;
  Integer pv;

  public User(String name, Integer pv) {
    this.name = name;
    this.pv = pv;
  }
}
