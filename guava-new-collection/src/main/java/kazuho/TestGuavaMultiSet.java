package kazuho;


import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TestGuavaMultiSet {
  public static void main(String[] args) {
    //  traditional way
    String[] words = {"w", "w", "w", ".", "g", "o", "o", "g", "l", "e", ".", "c", "o", "m",};
    Map<String, Integer> counts = wordCountByHashMap(words);
    System.out.println("word 'g' appear: " + counts.get("g"));

    //  multiset way
    Multiset<String> statistics = wordCountByMultiSet(words);
    System.out.println("word 'g' appear: " + statistics.count("g"));

    //  toSet
    Set<String> wordSet = statistics.elementSet();
    System.out.println(wordSet.toString());

    //  use entry
    Set<Multiset.Entry<String>> entrySet =  statistics.entrySet();
    for (Multiset.Entry<String> entry : entrySet){
      System.out.println("letter: " + entry.getElement() + " appear: " + entry.getCount());
    }
  }

  public static Map<String, Integer> wordCountByHashMap(String[] words){
    Map<String, Integer> counts = new HashMap<>();

    for (String word : words) {
      Integer count = counts.get(word);
      if (count == null) {
        counts.put(word, 1);
      } else {
        counts.put(word, count + 1);
      }
    }

    return counts;
  }

  public static Multiset<String> wordCountByMultiSet(String[] words){
    Multiset<String> counts = HashMultiset.create();

    for (String word : words){
      counts.add(word);
    }

    return counts;
  }
}