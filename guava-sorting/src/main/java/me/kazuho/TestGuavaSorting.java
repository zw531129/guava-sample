package me.kazuho;

import com.google.common.collect.Ordering;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class TestGuavaSorting {
  public static void main(String[] args) {

    Ordering order = Ordering.natural();
    List<Integer> ints = new ArrayList<>();
    generateRandom(ints);

    //  sort by asc
    Collections.sort(ints, order);
    System.out.println(ints.toString());

    //  get leastest ones
    List<Integer> leasts = order.leastOf(ints, 3);
    System.out.println(leasts.toString());

    //  get min value
    Integer minVal = (Integer) order.min(ints);
    System.out.println(minVal);

    //  sort by desc
    Ordering reserve = order.reverse();
    Collections.sort(ints, reserve);
    System.out.println(ints.toString());

    //  get greatest ones
    List<Integer> greatests = order.greatestOf(ints, 3);
    System.out.println(greatests.toString());

    //  get max value
    Integer maxVal = (Integer) order.max(ints);
    System.out.println(maxVal);

    List<Character> chars = new ArrayList<>();
    generateStringWithNull(chars);

    //  nulls at last
    Ordering nullsLast = order.nullsLast();
    Collections.sort(chars, nullsLast);
    System.out.println(chars.toString());

  }

  public static List<Integer> generateRandom(List<Integer> ints){
    Random random = new Random(System.currentTimeMillis());
    for (int i = 0; i < 10; i++){
      ints.add(random.nextInt());
    }

    return ints;
  }

  public static List<Character> generateStringWithNull(List<Character> strings){
    String chars = "abcdefghijklmnopqrstuvwxyz";
    for (int i = 0; i < 26; i++){
      Double index = Math.random() * 26;
      strings.add(chars.charAt(index.intValue()));
    }

    Double randomIdx = Math.random() * 10;
    strings.add(randomIdx.intValue(), null);

    return strings;
  }
}
